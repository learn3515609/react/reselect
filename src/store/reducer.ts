import { combineReducers } from '@reduxjs/toolkit';
import { todosReducer } from '../todos/services/reducer';

export const rootReducer = combineReducers({
  todos: todosReducer
})