import { createReducer, PayloadAction } from '@reduxjs/toolkit'
import { 
  TODOS_ADD_ITEM, TODOS_CHANGE_STATUS, TODOS_CHANGE_FILTER, 
  TTodosAddPayload, TTodosChangeStatusPayload, TTodosChangeFilterPayload 
} from './actions'

export type TTodoStatus = 'todo' | 'active' | 'done';

export type TTodo = {
  id: number,
  name: string,
  status: TTodoStatus
}

type TTodosState = {
  list: TTodo[],
  filter?: TTodoStatus
}

const todosInitialState: TTodosState = {
  list: []
}

export const todosReducer = createReducer(todosInitialState, {
  [TODOS_ADD_ITEM.type]: (state, action: PayloadAction<TTodosAddPayload>) => {
    state.list.push(action.payload.todo)
  },
  [TODOS_CHANGE_STATUS.type]: (state, action: PayloadAction<TTodosChangeStatusPayload>) => {
    const { id, status } = action.payload;
    const item = state.list.find(todo => todo.id === id);
    if(item) item.status = status;
  },
  [TODOS_CHANGE_FILTER.type]: (state, action: PayloadAction<TTodosChangeFilterPayload>) => {
    state.filter = action.payload.filter;
  }
})