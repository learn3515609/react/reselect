import { createAction } from '@reduxjs/toolkit';
import { TTodo, TTodoStatus } from './reducer';

export type TTodosAddPayload = {
  todo: TTodo
}
export type TTodosChangeStatusPayload = {
  id: number,
  status: TTodoStatus
}
export type TTodosChangeFilterPayload = {
  filter?: TTodoStatus
}

export const TODOS_ADD_ITEM = createAction<TTodosAddPayload>('todos/ADD');
export const TODOS_CHANGE_STATUS = createAction<TTodosChangeStatusPayload>('todos/CHANGE_STATUS');
export const TODOS_CHANGE_FILTER = createAction<TTodosChangeFilterPayload>('todos/CHANGE_FILTER');