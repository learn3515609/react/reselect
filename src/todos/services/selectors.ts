import { createSelector } from '@reduxjs/toolkit';
import { RootState } from '../../store/types';

export const todosAllListSelector = (state: RootState) => state.todos.list
export const todosFilterSelector = (state: RootState) => state.todos.filter

export const todosByFilterSelector = createSelector(
  [todosAllListSelector, todosFilterSelector],
  (todos, filter) => {
    switch(filter) {
      case 'todo': return todos.filter(todo => todo.status === 'todo');
      case 'active': return todos.filter(todo => todo.status === 'active');
      case 'done': return todos.filter(todo => todo.status === 'done');
      default: return todos;
    }
  }
)