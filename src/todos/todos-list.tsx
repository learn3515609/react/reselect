import { useAppSelector, useAppDispatch } from '../store/hooks';
import { TODOS_CHANGE_FILTER } from './services/actions';
import { TTodoStatus } from './services/reducer';
import { todosByFilterSelector } from './services/selectors';
import { TodoItem } from './todos-item';

export const TodosList = () => {
  const list = useAppSelector(todosByFilterSelector);
  const dispatch = useAppDispatch();

  const onFilterChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const filter = e.target.value as TTodoStatus;
    dispatch(TODOS_CHANGE_FILTER({ filter }))
  }
  
  return <div style={{marginTop: 10}}>
    show items: <select onChange={onFilterChange}>
      <option value={undefined}>all</option>
      <option>todo</option>
      <option>active</option>
      <option>done</option>
    </select>
    <ul>
      {list.map(item => <TodoItem item={item} />)}
    </ul>
  </div>
}
