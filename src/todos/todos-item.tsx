import { useAppDispatch } from '../store/hooks'
import { TODOS_CHANGE_STATUS } from './services/actions';
import { TTodo, TTodoStatus } from './services/reducer'

export const TodoItem = ({
  item
}: {
  item: TTodo
}) => {
  const dispatch = useAppDispatch();

  const changeStatus = (status: TTodoStatus) => {
    dispatch(TODOS_CHANGE_STATUS({ id: item.id, status}));
  }

  return <li>
    <TodoItemStatus status={item.status} /> 
    {item.name} 
    <button onClick={changeStatus.bind(null, 'todo')}>todo</button>
    <button onClick={changeStatus.bind(null, 'active')}>active</button>
    <button onClick={changeStatus.bind(null, 'done')}>done</button>
  </li>
}

const TodoItemStatus = ({
  status
}: {
  status: TTodoStatus
}) => {
  switch(status) {
    case 'todo':
      return <>⌛</>;
    case 'active':
      return <>✍</>;
    case 'done':
      return <>✅</>;
  }
}