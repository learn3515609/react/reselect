import { useState } from 'react';
import { useAppDispatch } from '../store/hooks';
import { TODOS_ADD_ITEM } from './services/actions';
import { TTodo } from './services/reducer';

export const TodosAddForm = () => {
  const [value, setValue] = useState('');
  const dispatch = useAppDispatch();

  const onSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    const todo: TTodo = {
      id: Date.now(),
      name: value,
      status: 'todo'
    }
    dispatch(TODOS_ADD_ITEM({todo}));
    setValue('');
  }

  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setValue(e.target.value);
  }

  return <div>
    <form onSubmit={onSubmit}>
      <input 
        placeholder="input name of new task"
        value={value}
        onChange={onChange}
      />
    </form>
  </div>
}