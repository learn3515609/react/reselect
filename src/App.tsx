import React from 'react';
import { TodosAddForm } from './todos/todos-add-form';
import { TodosList } from './todos/todos-list';
import { Provider } from 'react-redux' 
import store from './store';

function App() {
  return (
    <Provider store={store}>
      <TodosAddForm />
      <TodosList />
    </Provider>
  );
}

export default App;
